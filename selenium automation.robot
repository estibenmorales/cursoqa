*** Settings ***
Library     String
Library     SeleniumLibrary

*** Variables ***
${browser}      chrome
${homepage}     automationpractice.com/index.php
${scheme}       http
${prodScheme}   https
${testurl}      ${scheme}://${homepage}
${produrl}      ${prodScheme}://${homepage}

*** KeyWords ***
Open Homepage
    Open Browser    ${testurl}      ${browser}

*** Test Case ***
001 Hacer clic en Contenedores
    Open Homepage
    Set Global Variable     @{nombresDeCotenedores}     //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    FOR    ${nombredeContenedor}   IN      @{nombresDeCotenedores}
       Click Element   xpath=${nombredeContenedor}
       Wait Until Element Is Visible      xpath=//*[@id="bigpic"]
       Click Element   xpath=//*[@id="header_logo"]/a/img
    END
    Close Browser
